console.log(document);
console.log(document.querySelector("#text-first-name")); //result - element from html

/*
	document - refers to the whole webpage
	querySelector - used to select a specific element (object) as long as it is inside the html tag(HTML ELEMENT);
		- takes a string input htat is formatted like CSS Selector
		- can slect elements regardless if the string is an id, a class, or a tag as long as the element is existing in the webpage.
*/

const txtFirstName = document.querySelector("#text-first-name");
const txtLastName = document.querySelector("#text-last-name");
const spanFullName = document.querySelector("#span-full-name");

/*
	event
	actions that the user is doing in our webpage (scroll, click, hover, keypress/type)

	addEventListener
	function that lers the webpage to listen to the events performed by the user.
		takes two arguments
			- string - the event to which the HTML element will listen. These are predetermined
			- function - executed by the element once the event (first argument) is triggered.
*/

txtFirstName.addEventListener("keyup", (event)=> {
	spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener("keyup", (event)=> {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
});

/*

	mini activity
	- make another keyup event where in the span element will record the last name in the forms.
	- Send screen shot.

	solution:
		txtLastName.addEventListener("keyup", (event)=> {
			spanFullName.innerHTML = txtLastName.value
		});
*/